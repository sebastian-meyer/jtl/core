<?php

declare(strict_types=1);

namespace Tests\Services\JTL\Consent\Statistics;

use JTL\Consent\Statistics\Repositories\ConsentStatisticsRepository;
use JTL\Consent\Statistics\Services\ConsentStatisticsService;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\UnitTestCase;

/**
 * @covers  \JTL\Consent\Statistics\Services\ConsentStatisticsService
 * @comment This class tests the ConsentStatisticsService functions
 */
class ConsentStatisticsServiceTest extends UnitTestCase
{
    private ConsentStatisticsService $consentStatisticsService;
    private ConsentStatisticsRepository&MockObject $consentStatisticsRepository;

    /**
     * This method is called before each test.
     *
     * @throws \Exception
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->consentStatisticsRepository = $this->createMock(ConsentStatisticsRepository::class);
        $this->consentStatisticsService    = new ConsentStatisticsService(
            consentStatisticsRepository: $this->consentStatisticsRepository,
        );
    }

    /**
     * @return object[]
     */
    private function consentValues(): array
    {
        return [
            \date('Y-m-d', \strtotime('-5 days')) => (object)[
                'visitors'    => 8,
                'acceptedAll' => 6,
                'consents'    => [
                    'youtube' => 7,
                    'vimeo'   => 6,
                ],
            ],
            \date('Y-m-d', \strtotime('-4 days')) => (object)[
                'visitors'    => 3,
                'acceptedAll' => 1,
                'consents'    => [
                    'youtube' => 2,
                    'vimeo'   => 1,
                ],
            ],
            \date('Y-m-d', \strtotime('-3 days')) => (object)[
                'visitors'    => 22,
                'acceptedAll' => 17,
                'consents'    => [
                    'youtube' => 18,
                    'vimeo'   => 17,
                ],
            ],
            \date('Y-m-d', \strtotime('-2 days')) => (object)[
                'visitors'    => 11,
                'acceptedAll' => 10,
                'consents'    => [
                    'youtube' => 10,
                    'vimeo'   => 11,
                ],
            ],
            \date('Y-m-d', \strtotime('-1 days')) => (object)[
                'visitors'    => 8,
                'acceptedAll' => 8,
                'consents'    => [
                    'youtube' => 8,
                    'vimeo'   => 8,
                ],
            ],
            \date('Y-m-d') => (object)[
                'visitors'    => 5,
                'acceptedAll' => 1,
                'consents'    => [
                    'youtube' => 1,
                    'vimeo'   => 5,
                ],
            ],
        ];
    }

    /**
     * @return void
     */
    public function testGetConsentStats(): void
    {
        $this->consentStatisticsRepository
            ->method('getConsentValues')
            ->willReturn($this->consentValues());

        $consentStats = $this->consentStatisticsService->getConsentStats(
            \date('Y-m-d', \strtotime('-4 days')),
            \date('Y-m-d'),
            ['youtube', 'vimeo']
        );

        $this->assertObjectHasProperty('dataTable', $consentStats);
        $this->assertObjectHasProperty('dataChart', $consentStats);

        $this->assertCount(6, $consentStats->dataTable);
        $this->assertCount(6, $consentStats->dataChart);

        $this->assertEquals(
            \date('Y-m-d', \strtotime('-5 days')),
            $consentStats->dataTable[0]->date
        );
    }

    /**
     * @return void
     */
    public function testHasAcceptedAll(): void
    {
        $resultAllTrue = $this->consentStatisticsService->hasAcceptedAll(
            [
                'youtube' => true,
                'vimeo'   => true,
                'google'  => true,
            ]
        );

        $resultNotAllTrue = $this->consentStatisticsService->hasAcceptedAll(
            [
                'youtube' => true,
                'vimeo'   => false,
                'google'  => true,
            ]
        );

        $this->assertTrue($resultAllTrue);
        $this->assertNotTrue($resultNotAllTrue);
    }
}
