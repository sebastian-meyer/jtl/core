# HOOK_BESTELLUNGEN_XML_DELETEORDER

## Triggerpunkt

In `JTL\dbeS\Sync\Orders::deleteOrder` vor der Löschung einer Bestellung beim Abgleich mit JTL-Wawi.

## Parameter

* `int` **orderId** - die ID der Bestellung