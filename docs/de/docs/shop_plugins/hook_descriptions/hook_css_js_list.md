# HOOK_CSS_JS_LIST (204)

## Triggerpunkt

Liste aller JS/CSS Gruppen, um sie nach ihrer Erzeugung zu minimieren (via `Template::getMinifyArray()`)

## Parameter

* `array` **&groups** - Liste der tpl-Gruppen
* `array` **&cache_tags** - Liste der zugewiesenen Cache-IDs