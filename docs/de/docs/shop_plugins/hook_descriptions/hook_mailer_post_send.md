# HOOK_MAILER_POST_SEND (292)

## Triggerpunkt

Direkt nach dem Senden einer Email via PHPMailer

## Parameter

* `\JTL\Mail\Mailer` **mailer** - Mailer-Objekt
* `\JTL\Mail\Mail\MailInterface` **mail** - Mail-Objekt
* `\PHPMailer\PHPMailer\PHPMailer` **phpmailer** - PHPMailer-Instanz
* `bool` **status** - TRUE wenn erfolgreich versendet, FALSE sonst