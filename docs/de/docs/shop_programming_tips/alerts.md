# Alerts

*Alerts* (z.B. Fehler-, Hinweis- oder Erfolgsmeldungen) werden ab JTL-Shop 5.0 mit dem Alert-Service erzeugt.
Sie können im sowohl Shop-Frontend als auch im Adminbereich angezeigt werden.

Eine Referenz zum *Alert*-Service erhalten Sie über die Shop-Klasse:

    <?php
    
    use JTL\Services\JTL\AlertServiceInterface;
    
    $alertService = Shop::Container()->getAlertService();

Zum Erzeugen einer neuen Alert-Meldung nutzen Sie die Methode `\JTL\Services\JTL\AlertService::addAlert` !

Syntax:

    addAlert(string $type, string $message, string $key, array $options = null): ?Alert;

`$message` bestimmt den Text, der in der Alertbox enthalten ist.

`$type` legt einen Anzeigetyp fest. Diese Anzeigetypen entsprechen den Klassennamen der Bootstrap Alert-Komponente
([siehe Bootstrap-Dokumentation](https://getbootstrap.com/docs/4.6/components/alerts/)).

Folgende Werte stehen als Konstanten aus der Klasse `\JTL\Alert\Alert` zur Verfügung:

| Konstante | Werte |
| - | - |
| `TYPE_PRIMARY` | *primary* |
| `TYPE_SECONDARY` | *secondary* |
| `TYPE_SUCCESS` | *success* |
| `TYPE_DANGER` | *danger* |
| `TYPE_WARNING` | *warning* |
| `TYPE_INFO` | *info* |
| `TYPE_LIGHT` | *light* |
| `TYPE_DARK` | *dark* |
| `TYPE_ERROR` | *error* (ehemals für ``$cFehler`` genutzt) |
| `TYPE_NOTE` | *note* (ehemals für ``$cHinweis`` genutzt) |

Für die wichtigsten Anzeigetypen empfehlen wir die Nutzung der entsprechenden Wrapper-Methoden:

    addError(string $message, string $key, array $options = null): ?Alert
    addWarning(string $message, string $key, array $options = null): ?Alert
    addInfo(string $message, string $key, array $options = null): ?Alert
    addSuccess(string $message, string $key, array $options = null): ?Alert
    addDanger(string $message, string $key, array $options = null): ?Alert
    addNotice(string $message, string $key, array $options = null): ?Alert

Mit dem Parameter `$key` legen Sie einen eindeutigen Identifikator fest, über den das Alert identifiziert und ggf.
überschrieben werden kann. `$key` wird zudem im HTML-Attribut `data-key` des Alert-Elements hinterlegt damit es per
Javascript oder CSS angesteuert werden kann.

Für den optionalen Parameter `$options` siehe weiter unten.

## Beispiel

Dieses Beispiel erzeugt eine simple Alertbox vom Typ `info` und weißt diesem den Schlüssel `testInfo` zu:

    <?php
    
    $alertService->addInfo('Das ist eine Testinfo!', 'testInfo');

## Optionen

Der optionalen Parameter `$options` erlaubt einige Feineinstellungen. Diese werden als assoziatives Array an die
Methode übergeben.

Alle möglichen Optionen:


| Option | Typ | Standardwert | Beschreibung |
| - | - | - | - |
| `dismissable` | bool | false | Alert kann vom Nutzer ausgeblendet werden |
| `fadeOut` | int | 0 | Zeit in Millisekunden wie lange der Ausblendeffekt des Alerts dauert. |
| `showInAlertListTemplate` | bool | true | Alert an zentraler Stelle im Header ausgeben |
| `saveInSession` | bool | false | Alert in der *SESSION* speichern (z. B. für Redirects) |
| `linkHref` | string | | URL, falls ganzes Alert als Link dargestellt werden soll |
| `linkText` | string | | Wenn `linkHref` und `linkText` gesetzt sind, wird an die Message der Text als Link angehängt |
| `icon` | string | | *Fontawesome*-Icon |
| `id` | string | | Fügt dem HTML-Element des Alerts ein `id`-Attribut hinzu |

**Beispiel**

Mit diesem Aufruf wird ein Info-Alert erzeugt, welches einen zusätzlichen Button zum Ausblenden erhält:

    <?php

    $alertHelper->addAlert(
        Alert::TYPE_INFO,
        'Das ist eine Testinfo!',
        'testInfo',
        ['dismissable' => true]
    );

## Anzeige im Frontend

Die Alerts sind in der Smarty-Variable `alertList` als Collection gespeichert. Alle Alerts, bei denen
`showInAlertListTemplate === true` gesetzt ist, werden zentral im Header ausgegeben.

    {include file='snippets/alert_list.tpl'}

Falls Sie ein Alert statt im Header an einer speziellen Stelle in einem Template ausgeben lassen wollen, dann setzen
Sie die Option `showInAlertListTemplate` auf `false`. Geben Sie dann das *Alert* an gewünschter Stelle wie folgt aus:

    {$alertList->displayAlertByKey('testInfo')}
